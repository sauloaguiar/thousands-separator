class RegexThousandSeparator {
  format(number, separator = ',') {
    if (!Number.isInteger(number)) {
      return '0';
    }
    return String(number).replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${separator}`);
  }
}
module.exports = RegexThousandSeparator