const assert = require('chai').assert;
const MathThousandSeparator = require('./MathThousandsSeparator');
const RegexThousandoSeparator = require('./RegexThousandSeparator');
const StringThousandSeparator = require('./StringThousandSeparator');

function testImplementation(implementation, label) {
  describe(`format() for ${label}`, function() {
    const separator = implementation;
    it('should return 1,234 for 1234 value', function() {
      assert.equal(separator.format(1234), '1,234');
    });

    it('should return 987,654,321 for 987654321 value', function() {
      assert.equal(separator.format(987654321), '987,654,321');
    });

    it('should return 123 for 123 value', function() {
      assert.equal(separator.format(123), '123');
    });

    it('should return 9,876,543,210 for 9876543210 value', function() {
      assert.equal(separator.format(9876543210), '9,876,543,210');
    });

    it('should return 0 for invalid entry value', function() {
      assert.equal(separator.format(null), '0');
      assert.equal(separator.format(''), '0');
      assert.equal(separator.format(true), '0');
      assert.equal(separator.format(undefined), '0');
    });

    it('should keep minus sign for negative entry value', function() {
      assert.equal(separator.format(-9876543210), '-9,876,543,210');
    });

    it('should work with an specific separator', function() {
      assert.equal(separator.format(-9876543210, '#'), '-9#876#543#210');
    });
  });
}

describe('Thousands Separator', function() {
  function createMathSeparator() {
    return new MathThousandSeparator();
  }

  function createRegexSeparator() {
    return new RegexThousandoSeparator();
  }

  function createStringSeparator() {
    return new StringThousandSeparator();
  }

  testImplementation(createMathSeparator(), 'math');
  testImplementation(createRegexSeparator(), 'regex');
  testImplementation(createStringSeparator(), 'string');
});