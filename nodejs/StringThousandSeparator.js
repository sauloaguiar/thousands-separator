class StringThousandSeparator {
  format(number, separator = ',') {
    if (!Number.isInteger(number)) {
      return '0';
    }

    const sNumber = new String(number);
    let position = 0;
    let resultNumber = '';
    for (let i = sNumber.length; i >= 0; i--) {
      resultNumber = sNumber.charAt(i) + resultNumber;
      if (position != 0 && position != sNumber.length && position % 3 == 0) {
        resultNumber = separator + resultNumber;
      }
      position++;
      
    }
    return resultNumber;
  }
}

module.exports = StringThousandSeparator