class MathThousandSeparator {
  format(number, separator = ',') {
    if (!Number.isInteger(number)) {
      return '0';
    }
    
    let stringResult = '';
    let absNumber = Math.abs(number);
    while (absNumber > 1000) {
      stringResult = separator + absNumber % 1000 + stringResult;
      absNumber = Math.floor(absNumber / 1000);
    }
    stringResult = absNumber + stringResult;
    if (number < 0) {
      stringResult = '-' + stringResult;
    }
    return stringResult;
  }
}

module.exports = MathThousandSeparator