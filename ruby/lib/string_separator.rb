class StringSeparator
  def format(number, separator = ',')
    if (number == nil || number == false || number == true || number == '')
      return '0'
    end
    sNumber = number.to_s
    i = sNumber.length - 1
    position = 0
    resultNumber = ''
    while (i >= 0) do
      resultNumber = sNumber[i] + resultNumber
      i -= 1
      position += 1
      if (position != 0 && position != sNumber.length && position % 3 == 0)
        resultNumber = separator + resultNumber
      end
    end
    return resultNumber
  end
end
