class MathSeparator
  def format(number, separator = ',')
    if (number == nil || number == false || number == true || number == '')
      return '0'
    end
    stringResult = ''
    absNumber = number.abs
    while (absNumber > 1000) do
      stringResult = separator + (absNumber % 1000).to_s + stringResult
      absNumber = (absNumber/1000).floor
    end

    stringResult = absNumber.to_s + stringResult
    if (number < 0)
      stringResult = "-" + stringResult
    end
    return stringResult
  end
end