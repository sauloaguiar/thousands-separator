class RegexSeparator
  def format(number, separator = ',')
    if (number == nil || number == false || number == true || number == '')
      return '0'
    end
    
    return number.to_s.gsub(/(\d)(?=(\d{3})+(?!\d))/, "\\1#{separator}")
  end
end