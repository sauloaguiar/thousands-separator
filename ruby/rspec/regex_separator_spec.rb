require 'rspec'
require_relative  '../lib/regex_separator.rb'

describe RegexSeparator do
  describe '#format' do
    it 'should return "1,234" for 1234 input' do
      expect(subject.format(1234)).to eql('1,234')
    end

    it 'should return 123,456 for 123456 input' do
      expect(subject.format(123456)).to eql('123,456')
    end

    it 'should return 123 for 123 input' do
      expect(subject.format(123)).to eql('123')
    end

    it 'should return 9,876,543,210 for 9876543210 input' do
      expect(subject.format(9876543210)).to eql('9,876,543,210')
    end

    it 'should return 0 for invalid input' do
      expect(subject.format(nil)).to eql('0')
      expect(subject.format('')).to eql('0')
      expect(subject.format(true)).to eql('0')
      expect(subject.format(false)).to eql('0')
    end

    it 'should return -1,234 for -1234 input' do
      expect(subject.format(-1234)).to eql('-1,234')
    end

    it 'should return -1#234 for -1234, # input' do
      expect(subject.format(-1234, '#')).to eql('-1#234')
    end
  end
end